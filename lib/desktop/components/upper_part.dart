import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:test_page/constants.dart';

class UpperPartWidget extends StatelessWidget {
  const UpperPartWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyCustomCLipperPath(),
      child: Container(
        width: double.infinity,
        height: 659.37,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp,
            transform: GradientRotation(102),
          ),
        ),
        // child: const Text('No idea'),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 320,
              height: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Diene job \nwebsite',
                    style: TextStyle(
                      fontFamily: 'Lato',
                      fontWeight: FontWeight.normal,
                      fontSize: 65,
                      height: 78 / 65,
                      letterSpacing: 1.95,
                      color: Color(0xff2D3748),
                    ),
                    textAlign: TextAlign.left,
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Container(
                      alignment: Alignment.center,
                      width: 320,
                      height: 40,
                      decoration: const BoxDecoration(
                        gradient: sAppBarGradient,
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                      ),
                      child: const Text(
                        'Kostenlos Registrieren',
                        style: TextStyle(
                            color: Color(0xFFE6FFFA), fontFamily: 'Lato'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 200,
            ),
            SizedBox(
              width: 455,
              height: 455,
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 60,
                child: ClipRRect(
                  clipBehavior: Clip.hardEdge,
                  // clipper: ,
                  borderRadius: BorderRadius.circular(60),
                  child: Image.asset(
                    'assets/images/hands.png',
                    fit: BoxFit.cover,
                    width: 400,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyCustomCLipperPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;

    Path path0 = Path();

    path0.moveTo(0, size.height * 0.9833333);
    path0.quadraticBezierTo(size.width * 0.0989583, size.height * 0.9908333,
        size.width * 0.2341667, size.height * 0.9800000);
    path0.cubicTo(
        size.width * 0.3958333,
        size.height * 0.9675000,
        size.width * 0.4270833,
        size.height * 0.9450000,
        size.width * 0.5758333,
        size.height * 0.9400000);
    path0.cubicTo(
        size.width * 0.6762500,
        size.height * 0.9304167,
        size.width * 0.6237500,
        size.height * 0.9345833,
        size.width * 0.7608333,
        size.height * 0.9150000);
    path0.quadraticBezierTo(size.width * 0.8612500, size.height * 0.9000000,
        size.width * 0.9025000, size.height * 0.8950000);
    path0.lineTo(size.width, size.height * 0.8766667);
    path0.lineTo(size.width, 0);
    path0.lineTo(0, 0);
    path0.lineTo(0, size.height * 0.9833333);
    path0.close();

    return path0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
