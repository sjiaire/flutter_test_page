import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/tabs_controller.dart';
import '../../size_constants.dart';

class SectionTwoPhone extends StatelessWidget {
  SectionTwoPhone({
    Key? key,
  }) : super(key: key);
  TabsController tabs_controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: ClipPath(
        clipper: MyCustomCLipperPath(),
        child: Container(
          alignment: Alignment.centerLeft,
          width: double.infinity,
          height: 360,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp,
              transform: GradientRotation(102),
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                left: -20,
                top: -30,
                child: Stack(
                  children: [
                    SizedBox(
                      width: 360,
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 104,
                        child: ClipRRect(
                          // clipper: ,
                          borderRadius: BorderRadius.circular(60),
                          child: const Text(
                            '2.',
                            style: TextStyle(
                                fontSize: 130, color: Color(0xFF718096)),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 55,
                      right: -55,
                      child: Obx(() => Text(
                            tabs_controller.tab.value == 1
                                ? 'Erstellen dein Lebenslauf'
                                : tabs_controller.tab.value == 2
                                    ? 'Erstellen ein Jobinserat'
                                    : tabs_controller.tab.value == 3
                                        ? 'Erhalte Vermittlungs- \nangebot von Arbeitgeber'
                                        : 'Erstellen dein Lebenslauf',
                            style: const TextStyle(
                                color: Color(0xFF718096), fontSize: 16),
                          )),
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 60,
                right: 20,
                child: Obx(() => Image.asset(
                      tabs_controller.tab.value == 1
                          ? 'assets/images/tasks.png'
                          : tabs_controller.tab.value == 2
                              ? 'assets/images/second_2.png'
                              : tabs_controller.tab.value == 3
                                  ? 'assets/images/third_2.png'
                                  : 'assets/images/tasks.png',
                      fit: BoxFit.cover,
                      width: 219,
                    )),
              ),
            ],
          ),
        ),
      ),
    );

    // ClipPath(
    //   clipper: MyCustomCLipperPath(),
    //   child: Container(
    // alignment: Alignment.centerLeft,
    // width: double.infinity,
    // height: 360,
    // decoration: const BoxDecoration(
    //   gradient: LinearGradient(
    //     begin: Alignment.topLeft,
    //     end: Alignment.bottomRight,
    //     colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
    //     stops: [0.0, 1.0],
    //     tileMode: TileMode.clamp,
    //     transform: GradientRotation(102),
    //   ),
    //     ),
    //     child: Column(
    //       crossAxisAlignment: CrossAxisAlignment.start,
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       children: [
    //         Container(
    //           color: Colors.red,
    //           child: Stack(
    //             children: [
    //               SizedBox(
    //                 width: double.infinity,
    //                 child: CircleAvatar(
    //                   backgroundColor: Colors.transparent,
    //                   radius: 104,
    //                   child: ClipRRect(
    //                     // clipper: ,
    //                     borderRadius: BorderRadius.circular(60),
    //                     child: const Text(
    //                       '2.',
    //                       style: TextStyle(
    //                           fontSize: 130, color: Color(0xFF718096)),
    //                     ),
    //                   ),
    //                 ),
    //               ),
    //               const Positioned(
    //                 bottom: 50,
    //                 right: 0,
    //                 child: Text(
    //                   'Erhalte Vermittlungs- \nangebot von Arbeitgeber',
    //                   style: TextStyle(color: Color(0xFF718096), fontSize: 16),
    //                 ),
    //               ),
    //             ],
    //           ),
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }
}

class MyCustomCLipperPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;

    Path path0 = Path();
    path0.moveTo(0, size.height * 0.0081081);
    path0.quadraticBezierTo(size.width * 0.1958333, size.height * -0.0277027,
        size.width * 0.5333333, size.height * 0.0837838);
    path0.quadraticBezierTo(
        size.width * 0.8319444, size.height * 0.1135135, size.width, 0);
    path0.quadraticBezierTo(size.width, size.height * 0.7013514, size.width,
        size.height * 0.9351351);
    path0.cubicTo(size.width * 0.6416667, size.height * 1.0750000,
        size.width * 0.4888889, size.height * 0.7925676, 0, size.height);
    path0.quadraticBezierTo(size.width * -0.0333333, size.height * 0.7770270, 0,
        size.height * 0.0081081);
    path0.close();

    return path0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
