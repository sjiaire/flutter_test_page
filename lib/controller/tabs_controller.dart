import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TabsController extends GetxController {
  var tab = 1.obs;
  var showArrows = true.obs;
}
