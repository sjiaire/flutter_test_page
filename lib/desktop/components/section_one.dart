import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/tabs_controller.dart';

class SectionOne extends StatelessWidget {
  SectionOne({
    Key? key,
  }) : super(key: key);
  TabsController tabs_controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Stack(
          children: [
            SizedBox(
              width: 800,
              child: CircleAvatar(
                backgroundColor: const Color(0xFFF7FAFC),
                radius: 104,
                child: ClipRRect(
                  // clipper: ,
                  borderRadius: BorderRadius.circular(60),
                  child: const Text(
                    '1.',
                    style: TextStyle(fontSize: 130, color: Color(0xFF718096)),
                  ),
                ),
              ),
            ),
            Obx(() => Positioned(
                  bottom: 50,
                  right: tabs_controller.tab.value != 1 ? 55 : 0,
                  child: Text(
                    tabs_controller.tab.value == 1
                        ? 'Erstellen dein Lebenslauf'
                        : tabs_controller.tab.value == 2
                            ? 'Erstellen dein \nUnternehmensprofil'
                            : tabs_controller.tab.value == 3
                                ? 'Erstellen dein \nUnternehmensprofil'
                                : 'Erstellen dein Lebenslauf',
                    style:
                        const TextStyle(color: Color(0xFF718096), fontSize: 30),
                  ),
                )),
          ],
        ),
        SizedBox(
          width: 50,
        ),
        Flexible(
          child: SizedBox(
            width: 400,
            child: Image.asset(
              'assets/images/girl.png',
              fit: BoxFit.cover,
              width: 395,
            ),
          ),
        ),
      ],
    );
  }
}
