import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_page/mobile/mobile_home.dart';

import 'controller/tabs_controller.dart';
import 'desktop/desktop_home.dart';
import 'theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  final tabs_controller = Get.put(TabsController());

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: const TextTheme(bodyText2: baseTextStyle),
      ),
      home: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth < 1100) {
            return MobileHome();
          } else {
            return DesktopHome();
          }
        },
      ),
    );
  }
}
