import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_page/size_constants.dart';

import '../constants.dart';
import '../controller/tabs_controller.dart';
import 'components/section_one.dart';
import 'components/section_two.dart';
import 'components/tabs.dart';

class MobileHome extends StatelessWidget {
  MobileHome({super.key});
  TabsController tabs_controller = Get.find();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      backgroundColor: const Color(0xFFFFFFFF),
      appBar: PreferredSize(
        preferredSize: const Size(double.infinity, 67),
        child: Column(children: [
          Container(
            height: 5,
            decoration: const BoxDecoration(
              gradient: sAppBarGradient,
            ),
          ),
          Container(
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x29000000),
                    offset: Offset(0, 3),
                    blurRadius: 6,
                  ),
                ]),
            alignment: Alignment.centerRight,
            height: 62,
            child: TextButton(
                onPressed: () {},
                child: const SizedBox(
                  width: 70,
                  child: Text(
                    'Login',
                    style: TextStyle(
                        color: Color(0xFF38B2AC),
                        fontFamily: 'Lato',
                        fontSize: 16
                        // decoration: TextDecoration.underline,
                        // color: Color(0xFF38B2AC),
                        // shadows: [
                        //   Shadow(color: Color(0xFF38B2AC), offset: Offset(0, -3))
                        // ],
                        // color: Colors.transparent,
                        // decoration: TextDecoration.underline,
                        // decorationColor: Color(0xFF38B2AC),
                        // decorationThickness: 2,
                        // decorationStyle: TextDecorationStyle.solid,
                        ),
                  ),
                )),
          )
        ]),
      ),
      bottomNavigationBar: Container(
        height: 88,
        width: double.infinity,
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
          ),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(0, -3),
              blurRadius: 6,
            )
          ],
        ),
        child: TextButton(
          onPressed: () {},
          child: Container(
            alignment: Alignment.center,
            width: 320,
            height: 40,
            decoration: const BoxDecoration(
              gradient: sAppBarGradient,
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            child: const Text(
              'Kostenlos Registrieren',
              style: TextStyle(color: Color(0xFFE6FFFA)),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SectionOnePhone(),
            const SizedBox(
              height: 50,
            ),
            TabsPhoneWidget(),
            const SizedBox(
              height: 50,
            ),
            SizedBox(
              child: Obx(() => Text(
                    tabs_controller.tab.value == 1
                        ? 'Drei einfache Schritte \nzu deinem neuen Job'
                        : tabs_controller.tab.value == 2
                            ? 'Drei einfache Schritte \nzu deinem neuen Mitarbeiter'
                            : tabs_controller.tab.value == 3
                                ? 'Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter'
                                : 'Drei einfache Schritte \nzu deinem neuen Job',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Color(0xFF2D3748),
                      fontSize: 21,
                    ),
                  )),
            ),
            const SizedBox(
              height: 50,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                alignment: Alignment.bottomCenter,
                height: getProportionateScreenHeight(300),
                width: double.infinity,
                child: Stack(
                  children: [
                    Obx(() => Positioned(
                          left: tabs_controller.tab.value == 1 ? -150 : -120,
                          bottom: 0,
                          child: Stack(
                            children: [
                              SizedBox(
                                width:
                                    tabs_controller.tab.value == 1 ? 440 : 360,
                                child: CircleAvatar(
                                  backgroundColor: const Color(0xFFF7FAFC),
                                  radius: 104,
                                  child: ClipRRect(
                                    // clipper: ,
                                    borderRadius: BorderRadius.circular(60),
                                    child: const Text(
                                      '1.',
                                      style: TextStyle(
                                          fontSize: 130,
                                          color: Color(0xFF718096)),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 50,
                                right: -20,
                                child: Text(
                                  tabs_controller.tab.value == 1
                                      ? 'Erstellen dein Lebenslauf'
                                      : tabs_controller.tab.value == 2
                                          ? 'Erstellen dein \nUnternehmensprofil'
                                          : tabs_controller.tab.value == 3
                                              ? 'Erstellen dein \nUnternehmensprofil'
                                              : 'Erstellen dein Lebenslauf',
                                  style: const TextStyle(
                                      color: Color(0xFF718096), fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        )),
                    Positioned(
                      top: 0,
                      right: 20,
                      child: Image.asset(
                        'assets/images/girl.png',
                        fit: BoxFit.cover,
                        width: 219,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 80,
            ),
            Container(
              height: 770,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  SectionTwoPhone(),
                  Positioned(
                    bottom: 50,
                    left: -200,
                    child: SizedBox(
                      width: 680,
                      child: CircleAvatar(
                        backgroundColor: const Color(0xFFF7FAFC),
                        radius: 215,
                        child: ClipRRect(
                          // clipper: ,
                          borderRadius: BorderRadius.circular(90),
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 140),
                            child: const Text(
                              '3.',
                              style: TextStyle(
                                  fontSize: 130, color: Color(0xFF718096)),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 280,
                    left: 200,
                    child: Obx(() => Text(
                          tabs_controller.tab.value == 1
                              ? 'Mit nur einem Klick \nbewerben'
                              : tabs_controller.tab.value == 2
                                  ? 'Wähle deinen \nneuen Mitarbeiter aus'
                                  : tabs_controller.tab.value == 3
                                      ? 'Vermittlung nach \nProvision oder \nStundenlohn'
                                      : 'Mit nur einem Klick \nbewerben',
                          style:
                              TextStyle(color: Color(0xFF718096), fontSize: 16),
                        )),
                  ),
                  Positioned(
                    bottom: 100,
                    child: Obx(() => Image.asset(
                          tabs_controller.tab.value == 1
                              ? 'assets/images/last.png'
                              : tabs_controller.tab.value == 2
                                  ? 'assets/images/second_3.png'
                                  : tabs_controller.tab.value == 3
                                      ? 'assets/images/deal.png'
                                      : 'assets/images/last.png',
                          fit: BoxFit.cover,
                          width: 230,
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
