import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants.dart';
import '../../controller/tabs_controller.dart';

class TabsPhoneWidget extends StatefulWidget {
  TabsPhoneWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<TabsPhoneWidget> createState() => _TabsPhoneWidgetState();
}

class _TabsPhoneWidgetState extends State<TabsPhoneWidget> {
  TabsController tabs_controller = Get.find();

  ScrollController _controller = ScrollController();

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 480,
      child: SingleChildScrollView(
        controller: _controller,
        scrollDirection: Axis.horizontal,
        child: Obx(
          () => Row(
            children: [
              TextButton(
                style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    minimumSize: const Size(50, 30),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    alignment: Alignment.centerLeft),
                onPressed: () {
                  _controller.animateTo(
                    0,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeInOut,
                  );
                  setState(() {
                    tabs_controller.tab.value = 1;
                  });
                },
                child: Container(
                  margin: const EdgeInsets.all(0),
                  padding: const EdgeInsets.all(0),
                  alignment: Alignment.center,
                  width: 160,
                  height: 32,
                  decoration: BoxDecoration(
                    border: tabs_controller.tab.value == 1
                        ? null
                        : Border.all(
                            color: const Color(0xFFCBD5E0), width: 1.0),
                    color: tabs_controller.tab.value == 1
                        ? const Color(0xFF81E6D9)
                        : Colors.transparent,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(12),
                      topLeft: Radius.circular(12),
                    ),
                  ),
                  child: Text(
                    'Arbeitnehmer',
                    style: TextStyle(
                        color: tabs_controller.tab.value == 1
                            ? const Color(0xFFE6FFFA)
                            : const Color(0xFF319795)),
                  ),
                ),
              ),
              TextButton(
                style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    minimumSize: Size(50, 30),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    alignment: Alignment.centerLeft),
                onPressed: () {
                  setState(() {
                    _controller.animateTo(
                      100,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInOut,
                    );
                    tabs_controller.tab.value = 2;
                  });
                },
                child: Container(
                  margin: const EdgeInsets.all(0),
                  padding: const EdgeInsets.all(0),
                  alignment: Alignment.center,
                  width: 160,
                  height: 32,
                  decoration: BoxDecoration(
                    border: tabs_controller.tab.value == 2
                        ? null
                        : Border.all(
                            color: const Color(0xFFCBD5E0), width: 1.0),
                    color: tabs_controller.tab.value == 2
                        ? Color(0xFF81E6D9)
                        : Colors.transparent,
                  ),
                  child: Text(
                    'Arbeitnehmer',
                    style: TextStyle(
                      color: tabs_controller.tab.value == 2
                          ? const Color(0xFFE6FFFA)
                          : const Color(0xFF319795),
                    ),
                  ),
                ),
              ),
              TextButton(
                style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    minimumSize: Size(50, 30),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    alignment: Alignment.centerLeft),
                onPressed: () {
                  setState(() {
                    _controller.animateTo(
                      200,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInOut,
                    );
                    tabs_controller.tab.value = 3;
                  });
                },
                child: Container(
                  margin: const EdgeInsets.all(0),
                  padding: const EdgeInsets.all(0),
                  alignment: Alignment.center,
                  width: 160,
                  height: 32,
                  decoration: BoxDecoration(
                    border: tabs_controller.tab.value == 3
                        ? null
                        : Border.all(
                            color: const Color(0xFFCBD5E0), width: 1.0),
                    color: tabs_controller.tab.value == 3
                        ? const Color(0xFF81E6D9)
                        : Colors.transparent,
                    borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(12),
                      topRight: Radius.circular(12),
                    ),
                  ),
                  child: Text(
                    'Temporärbüro',
                    style: TextStyle(
                      color: tabs_controller.tab.value == 3
                          ? const Color(0xFFE6FFFA)
                          : const Color(0xFF319795),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
