import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/tabs_controller.dart';

class SectionLast extends StatelessWidget {
  SectionLast({
    Key? key,
  }) : super(key: key);
  final tabs_controller = Get.put(TabsController());

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Stack(
          children: [
            SizedBox(
              width: 680,
              child: CircleAvatar(
                backgroundColor: const Color(0xFFF7FAFC),
                radius: 135,
                child: ClipRRect(
                  // clipper: ,
                  borderRadius: BorderRadius.circular(90),
                  child: Container(
                    margin: const EdgeInsets.only(bottom: 140),
                    child: const Text(
                      '3.',
                      style: TextStyle(fontSize: 130, color: Color(0xFF718096)),
                    ),
                  ),
                ),
              ),
            ),
            Obx(() => Positioned(
                  bottom: 135,
                  right: tabs_controller.tab.value == 3 ? 50 : 0,
                  child: Text(
                    tabs_controller.tab.value == 1
                        ? 'Mit nur einem Klick \nbewerben'
                        : tabs_controller.tab.value == 2
                            ? 'Wähle deinen \nneuen Mitarbeiter aus'
                            : tabs_controller.tab.value == 3
                                ? 'Vermittlung nach \nProvision oder \nStundenlohn'
                                : 'Mit nur einem Klick \nbewerben',
                    style: TextStyle(color: Color(0xFF718096), fontSize: 30),
                  ),
                )),
          ],
        ),
        const SizedBox(
          width: 50,
        ),
        Obx(() => Flexible(
              child: Image.asset(
                tabs_controller.tab.value == 1
                    ? 'assets/images/last.png'
                    : tabs_controller.tab.value == 2
                        ? 'assets/images/second_3.png'
                        : tabs_controller.tab.value == 3
                            ? 'assets/images/deal.png'
                            : 'assets/images/last.png',
                fit: BoxFit.cover,
                width: 395,
              ),
            )),
      ],
    );
  }
}
