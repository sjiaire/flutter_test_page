import 'package:flutter/material.dart';
import 'package:widget_arrows/widget_arrows.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool showArrows = true;

  @override
  Widget build(BuildContext context) => ArrowContainer(
    child: Scaffold(
      appBar: AppBar(
        title: Text('Widget Arrows Example'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const [
            ArrowElement(
              color: Colors.red,
              id: 'title',
              targetId: 'text2',
              targetAnchor: Alignment.topCenter,
              sourceAnchor: Alignment.bottomCenter,
              child: Text('Starts here'),
            ),
            SizedBox(height: 100),
            ArrowElement(
              id: 'text2',
              targetAnchor: Alignment.centerRight,
              child: Text(
                'Ends here',
              ),
            ),
          ],
        ),
      ),

    ),
  );
}