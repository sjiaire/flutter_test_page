import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:widget_arrows/widget_arrows.dart';
import 'package:flutter_svg/svg.dart';
import 'package:test_page/constants.dart';

import '../controller/tabs_controller.dart';
import '../size_constants.dart';
import 'components/section_last.dart';
import 'components/section_one.dart';
import 'components/section_two.dart';
import 'components/tabs.dart';
import 'components/upper_part.dart';

class DesktopHome extends StatefulWidget {
  DesktopHome({super.key});

  @override
  State<DesktopHome> createState() => _DesktopHomeState();
}

class _DesktopHomeState extends State<DesktopHome> {
  final tabs_controller = Get.put(TabsController());
  double sectionPosition = 430;
  bool sectionOutOfView = false;
  bool isHover = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: const Color(0xFFFFFFFF),
      appBar: PreferredSize(
        preferredSize: const Size(double.infinity, 67),
        child: Column(
          children: [
            Container(
              height: 5,
              decoration: const BoxDecoration(
                gradient: sAppBarGradient,
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x29000000),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    ),
                  ]),
              alignment: Alignment.centerRight,
              height: 62,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  if (sectionOutOfView)
                    const Text(
                      'Jetzt Klicken',
                      style: TextStyle(
                          // decoration: TextDecoration.underline,
                          color: Color(0xFF4A5568),
                          fontFamily: 'Lato',
                          fontSize: 16),
                    ),
                  const SizedBox(
                    width: 20,
                  ),
                  if (sectionOutOfView)
                    TextButton(
                      style: TextButton.styleFrom(
                          padding: EdgeInsets.zero,
                          minimumSize: Size(50, 30),
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          alignment: Alignment.centerLeft),
                      onPressed: () {},
                      child: Container(
                        margin: const EdgeInsets.all(0),
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        alignment: Alignment.center,
                        height: 32,
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: const Color(0xFFCBD5E0), width: 1.0),
                          color: Colors.transparent,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        child: const Text(
                          'Kostenlos Registrieren',
                          style: TextStyle(
                            color: const Color(0xFF319795),
                          ),
                        ),
                      ),
                    ),
                  const SizedBox(
                    width: 20,
                  ),
                  TextButton(
                    onPressed: () {},
                    onHover: (value) async {
                      setState(() {
                        isHover = true;
                      });
                      await Future.delayed(const Duration(seconds: 1));
                      setState(() {
                        isHover = false;
                      });
                    },
                    onFocusChange: (value) {
                      setState(() {
                        isHover = false;
                      });
                    },
                    child: SizedBox(
                      width: 70,
                      child: InkWell(
                        child: Text(
                          'Login',
                          style: isHover
                              ? const TextStyle(
                                  shadows: [
                                      Shadow(
                                          color: Color(0xFF38B2AC),
                                          offset: Offset(0, -3))
                                    ],
                                  decoration: TextDecoration.underline,
                                  color: Colors.transparent,
                                  decorationThickness: 2,
                                  fontFamily: 'Lato',
                                  decorationStyle: TextDecorationStyle.solid,
                                  decorationColor: Color(0xFF38B2AC),
                                  fontSize: 16
                                  // decoration: TextDecoration.underline,
                                  // color: Color(0xFF38B2AC),
                                  // color: Colors.transparent,
                                  )
                              : const TextStyle(
                                  color: Color(0xFF38B2AC),
                                  fontFamily: 'Lato',
                                  fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scrollInfo) {
          if (scrollInfo.metrics.pixels > sectionPosition &&
              !sectionOutOfView) {
            // section has gone out of view
            setState(() {
              sectionOutOfView = true;
            });
          } else if (scrollInfo.metrics.pixels < sectionPosition &&
              sectionOutOfView) {
            setState(() {
              sectionOutOfView = false;
            });
          }
          return true;
        },
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  UpperPartWidget(),
                  TabsWidget(),
                  const SizedBox(height: 50),
                  Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child: Obx(() => Text(
                          tabs_controller.tab.value == 1
                              ? 'Drei einfache Schritte \nzu deinem neuen Job'
                              : tabs_controller.tab.value == 2
                                  ? 'Drei einfache Schritte \nzu deinem neuen Mitarbeiter'
                                  : tabs_controller.tab.value == 3
                                      ? 'Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter'
                                      : 'Drei einfache Schritte \nzu deinem neuen Job',
                          style: const TextStyle(
                              color: Color(0xFF4A5568),
                              fontWeight: FontWeight.normal,
                              fontSize: 37,
                              // fontSize: 40,
                              letterSpacing: 0),
                        )),
                  ),
                  const SizedBox(height: 80),
                  SectionOne(),
                  const SizedBox(height: 80),
                  ClipPath(
                    clipper: SecondSectionClipper(),
                    child: Container(
                      height: 390,
                      width: double.infinity,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp,
                          transform: GradientRotation(104),
                        ),
                      ),
                      child: SectionTwo(),
                    ),
                  ),
                  const SizedBox(
                    height: 100,
                  ),
                  SectionLast(),
                  const SizedBox(
                    height: 80,
                  ),
                ],
              ),
              Positioned(
                top: 1100,
                left: 400,
                child: Container(
                  height: 300,
                  width: 400,
                  child: SvgPicture.asset('assets/icons/arrow_1.svg'),
                ),
              ),
              Positioned(
                top: 1500,
                left: 400,
                child: Container(
                  height: 300,
                  width: 400,
                  child: SvgPicture.asset('assets/icons/arrow_2.svg'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class SecondSectionClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;
    Path path0 = Path();
    path0.moveTo(0, size.height * 0.0864865);
    path0.quadraticBezierTo(size.width * 0.0731250, size.height * 0.0108108,
        size.width * 0.2083333, size.height * 0.0081081);
    path0.cubicTo(
        size.width * 0.2954167,
        size.height * 0.0155405,
        size.width * 0.3404167,
        size.height * 0.1837838,
        size.width * 0.7308333,
        size.height * 0.0918919);
    path0.quadraticBezierTo(size.width * 0.8683333, size.height * 0.0695946,
        size.width, size.height * 0.1648649);
    path0.lineTo(size.width, size.height * 0.8702703);
    path0.quadraticBezierTo(size.width * 0.9343750, size.height * 0.9148649,
        size.width * 0.8950000, size.height * 0.9297297);
    path0.cubicTo(
        size.width * 0.7989583,
        size.height * 0.9533784,
        size.width * 0.7127083,
        size.height * 0.9324324,
        size.width * 0.5091667,
        size.height * 0.9162162);
    path0.cubicTo(
        size.width * 0.3172917,
        size.height * 0.9337838,
        size.width * 0.2985417,
        size.height * 0.9716216,
        size.width * 0.1675000,
        size.height * 0.9891892);
    path0.quadraticBezierTo(size.width * 0.0541667, size.height * 0.9648649, 0,
        size.height * 0.8945946);
    path0.lineTo(0, size.height * 0.0864865);
    path0.close();

    return path0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
