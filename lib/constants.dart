import 'package:flutter/material.dart';

const sPrimaryColor1 = Color(0xFF3182CE);
const sPrimaryColor2 = Color(0xFF319795);
const sAppBarGradient = LinearGradient(
  colors: [Color(0xFF319795), Color(0xFF3182CE)],
);
