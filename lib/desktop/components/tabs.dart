import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants.dart';
import '../../controller/tabs_controller.dart';

class TabsWidget extends StatefulWidget {
  TabsWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<TabsWidget> createState() => _TabsWidgetState();
}

class _TabsWidgetState extends State<TabsWidget> {
  TabsController tabs_controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 480,
      child: Obx(
        () => Row(
          children: [
            TextButton(
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  minimumSize: Size(50, 30),
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  alignment: Alignment.centerLeft),
              onPressed: () {
                setState(() {
                  tabs_controller.tab.value = 1;
                });
              },
              child: Container(
                margin: const EdgeInsets.all(0),
                padding: const EdgeInsets.all(0),
                alignment: Alignment.center,
                width: 160,
                height: 32,
                decoration: BoxDecoration(
                  border: tabs_controller.tab.value == 1
                      ? null
                      : Border.all(color: const Color(0xFFCBD5E0), width: 1.0),
                  color: tabs_controller.tab.value == 1
                      ? const Color(0xFF81E6D9)
                      : Colors.transparent,
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    topLeft: Radius.circular(12),
                  ),
                ),
                child: Text(
                  'Arbeitnehmer',
                  style: TextStyle(
                      color: tabs_controller.tab.value == 1
                          ? const Color(0xFFE6FFFA)
                          : const Color(0xFF319795)),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  minimumSize: Size(50, 30),
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  alignment: Alignment.centerLeft),
              onPressed: () {
                setState(() {
                  tabs_controller.tab.value = 2;
                });
              },
              child: Container(
                margin: const EdgeInsets.all(0),
                padding: const EdgeInsets.all(0),
                alignment: Alignment.center,
                width: 160,
                height: 32,
                decoration: BoxDecoration(
                  border: tabs_controller.tab.value == 2
                      ? null
                      : Border.all(color: const Color(0xFFCBD5E0), width: 1.0),
                  color: tabs_controller.tab.value == 2
                      ? Color(0xFF81E6D9)
                      : Colors.transparent,
                ),
                child: Text(
                  'Arbeitnehmer',
                  style: TextStyle(
                    color: tabs_controller.tab.value == 2
                        ? const Color(0xFFE6FFFA)
                        : const Color(0xFF319795),
                  ),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  minimumSize: Size(50, 30),
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  alignment: Alignment.centerLeft),
              onPressed: () {
                setState(() {
                  tabs_controller.tab.value = 3;
                });
              },
              child: Container(
                margin: const EdgeInsets.all(0),
                padding: const EdgeInsets.all(0),
                alignment: Alignment.center,
                width: 160,
                height: 32,
                decoration: BoxDecoration(
                  border: tabs_controller.tab.value == 3
                      ? null
                      : Border.all(color: const Color(0xFFCBD5E0), width: 1.0),
                  color: tabs_controller.tab.value == 3
                      ? const Color(0xFF81E6D9)
                      : Colors.transparent,
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(12),
                    topRight: Radius.circular(12),
                  ),
                ),
                child: Text(
                  'Temporärbüro',
                  style: TextStyle(
                    color: tabs_controller.tab.value == 3
                        ? const Color(0xFFE6FFFA)
                        : const Color(0xFF319795),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
