import 'package:arrow_path/arrow_path.dart';
import 'package:flutter/material.dart';

class CurvedLine extends StatefulWidget {
  CurvedLine({Key? key}) : super(key: key);

  @override
  _CurvedLineState createState() => _CurvedLineState();
}

class _CurvedLineState extends State<CurvedLine> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Arrow Path Example'),
        ),
        body: ClipRect(
          child: CustomPaint(
            size: Size(MediaQuery.of(context).size.width, 700),
            painter: ArrowPainter(),
          ),
        ),
      );
}

class ArrowPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    /// The arrows usually looks better with rounded caps.
    final Paint paint = Paint()
      ..color = Colors.black
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..strokeJoin = StrokeJoin.miter
      ..blendMode = BlendMode.srcOver
      ..strokeWidth = 1.0;

    /// Use complex path.
    {
      Path path = Path();
      path.moveTo(size.width * 0.1241667, size.height * 0.1633333);
      path.quadraticBezierTo(size.width * 0.1272917, size.height * 0.4412500,
          size.width * 0.2891667, size.height * 0.4266667);
      path.quadraticBezierTo(size.width * 0.4512500, size.height * 0.4125000,
          size.width * 0.4566667, size.height * 0.7416667);
      path = ArrowPath.make(path: path, isDoubleSided: true);

      canvas.drawPath(path, paint..color = Colors.blue);

      final TextSpan textSpan = TextSpan(
        text: 'Complex path',
        style: TextStyle(color: Colors.blue),
      );
      final TextPainter textPainter = TextPainter(
        text: textSpan,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr,
      );
      textPainter.layout(minWidth: size.width);
      textPainter.paint(canvas, Offset(0, 168));
    }
  }

  @override
  bool shouldRepaint(ArrowPainter oldDelegate) => false;
}
