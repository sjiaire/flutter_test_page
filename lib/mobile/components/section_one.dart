import 'package:flutter/material.dart';

class SectionOnePhone extends StatelessWidget {
  const SectionOnePhone({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyCustomCLipperPath(),
      child: Container(
        width: double.infinity,
        height: 660,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp,
            transform: GradientRotation(102),
          ),
        ),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 20),
              child: const Text(
                'Deine Job \nwebsite',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFF2D3748),
                  fontSize: 42,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              height: 405,
              child: Image.asset(
                'assets/images/hands.png',
                fit: BoxFit.cover,
                width: 360,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MyCustomCLipperPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;

    Path path0 = Path();
    path0.moveTo(0, 0);
    path0.lineTo(0, size.height * 0.9650000);
    path0.quadraticBezierTo(size.width * 0.1170833, size.height * 0.8268750,
        size.width * 0.5033333, size.height * 0.9475000);
    path0.quadraticBezierTo(size.width * 0.7300000, size.height * 1.0128125,
        size.width, size.height * 0.8825000);
    path0.lineTo(size.width, 0);
    path0.lineTo(0, 0);
    path0.close();

    return path0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
