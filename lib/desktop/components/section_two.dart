import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/tabs_controller.dart';

class SectionTwo extends StatelessWidget {
  SectionTwo({
    Key? key,
  }) : super(key: key);
  TabsController tabs_controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Row(
        children: [
          const SizedBox(
            width: 30,
          ),
          Flexible(
            child: Image.asset(
              tabs_controller.tab.value == 1
                  ? 'assets/images/tasks.png'
                  : tabs_controller.tab.value == 2
                      ? 'assets/images/second_2.png'
                      : tabs_controller.tab.value == 3
                          ? 'assets/images/third_2.png'
                          : 'assets/images/tasks.png',
              fit: BoxFit.cover,
              width: 395,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          Stack(
            children: [
              SizedBox(
                width: 800,
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 104,
                  child: ClipRRect(
                    // clipper: ,
                    borderRadius: BorderRadius.circular(60),
                    child: const Text(
                      '2.',
                      style: TextStyle(fontSize: 130, color: Color(0xFF718096)),
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 50,
                right: 0,
                child: Text(
                  tabs_controller.tab.value == 1
                      ? 'Erstellen dein Lebenslauf'
                      : tabs_controller.tab.value == 2
                          ? 'Erstellen ein Jobinserat'
                          : tabs_controller.tab.value == 3
                              ? 'Erhalte Vermittlungs- \nangebot von Arbeitgeber'
                              : 'Erstellen dein Lebenslauf',
                  style: TextStyle(color: Color(0xFF718096), fontSize: 30),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
